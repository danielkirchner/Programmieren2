package kapitel1;

public class KartenspielTest {

	public static void main(String[] args) {
		Kartenspiel k1 = new Kartenspiel(5); 
		
		//Test der Funktionen von Kartenspiel
		System.out.println("Orignial:\t" + k1.toString());
		k1.mischen();
		System.out.println("Gemischt:\t" + k1.toString());
		k1.sort();
		System.out.println("Sortiert:\t" + k1.toString());
		k1.sort();
		System.out.println("Sortiert:\t" + k1.toString());

		Kartenspiel k2 = new Kartenspiel(3);
		k1.kartenSpielHinzufuegen(k2);
		System.out.println("Hinzugefuegt:\t" + k1.toString());

	}

}
