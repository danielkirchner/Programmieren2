package kapitel1;

import java.util.Arrays;

public class Kartenspiel {
	Karte k[];
			
	public Kartenspiel(int n)
	{
		k = new Karte[n];
		for(int i = 0;i<n;i++)
		{
			int farberand = (int) (Math.random()*4);
			int wertrand = (int) (Math.random()*8);
			k[i] = new Karte(Karte.Farbe.values()[farberand],Karte.Wert.values()[wertrand]);
		}
	}
	
	public String toString()
	{
		String s = "";
		for(Karte karte:k)
		{
			s += karte.toString() + "; ";
		}
		return s;
	}
	
	public void mischen()
	{
		for(int i = 0;i<k.length;i++)
		{
			int rand = (int)(Math.random()*k.length);
			Karte temp = k[i];
			k[i] = k[rand];
			k[rand] = temp;
			
		}
	}
	
	public void sort(){
		Arrays.sort(k);
	}
	
	public void kartenSpielHinzufuegen(Kartenspiel neu)
	{
		Karte[] kn = new Karte[k.length+neu.k.length];
		System.arraycopy(k,0,kn,0,k.length);
		System.arraycopy(neu.k,0,kn,k.length,neu.k.length);
		this.k = kn;
	}
	

	

}
