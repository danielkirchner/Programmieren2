package kapitel1;

public class Karte implements Comparable<Karte>{

	public enum Farbe{KARO,HERZ,PIK,KREUZ}
	public enum Wert{SIEBEN,ACHT,NEUN,ZEHN,BAUER,DAME,KOENIG,ASS}
	
//	public enum Farbe{w,x,y,z};
//	public enum Wert{a,b,c,d,e,f,g,h};
	Farbe f;
	Wert w;
	
	public Karte(Farbe f, Wert w)
	{
		this.f = f;
		this.w = w;
	}
	
	public String toString()
	{
		return "Farbe: " + f + ", Wert: "+ w;
	}
	

	@Override
	public int compareTo(Karte arg0) {
	
		if(arg0.getClass().getSimpleName().equals(this.getClass().getSimpleName())){
			Karte k2 = (Karte)(arg0);
			if(this.f.ordinal()>k2.f.ordinal())
			{
				return 1;
			}
			else if(this.f.ordinal()<k2.f.ordinal())
			{
				return -1;
			}
			return 0;
		}
		return -1;
	}
	
	public boolean equals(Object o)
	{
		if(o==null)
			return false;
		
		
		if(getClass()==o.getClass())
			return f==((Karte)o).f;
		
		return false;
	}
	
}
