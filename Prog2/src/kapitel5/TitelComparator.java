package kapitel5;

import java.util.Comparator;

public class TitelComparator implements Comparator<Buch>{


	@Override
	public int compare(Buch b1, Buch b2) {
		if(b1.getTitel().compareTo(b2.getTitel())==0)
		return 0;
		if(b1.getTitel().compareTo(b2.getTitel())>0)
		return 1;
		
		return -1;
		
	}

}
