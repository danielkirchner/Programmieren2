package kapitel5;

public class Buch{

	private String autor, isbn, titel;

	public Buch(String autor, String isbn, String titel) {
		this.autor = autor;
		this.isbn = isbn;
		this.titel = titel;
	}
	
	//toString
	public String toString(){
		return "Autor: " + autor + ", ISBN: "+ isbn + ", Titel:" + titel;
	}

	
	//Getters and Setters
	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

}
