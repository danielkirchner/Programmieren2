package kapitel5;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CountList<E> extends ArrayList<E> {

	public boolean add(E e) {
		return super.add(e);
	}

	public int count(E element) {
		int temp = 0;
		for (int i = 0; i < super.size(); i++) {
			if (super.get(i).equals(element))
				temp++;
		}
		return temp;
	}

	public int unique() {
		ArrayList<E> list = new ArrayList<E>();

		for (int i = 0; i < super.size(); i++)
			if (!list.contains(super.get(i)))
				list.add(super.get(i));

		return list.size();

	}

	public Map<E, Integer> counts() {
		HashMap<E, Integer> map = new HashMap<E, Integer>();
		for (int i = 0; i < super.size(); i++)
			if (!map.containsKey(super.get(i)))
				map.put(super.get(i), count(super.get(i)));

		return map;
	}

}