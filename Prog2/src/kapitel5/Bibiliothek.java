package kapitel5;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class Bibiliothek {

	ArrayList<Buch> liste;

	public Bibiliothek() {
		liste = new ArrayList<Buch>();
	}

	public boolean einfuegen(Buch b) {
		return liste.add(b);
	}

	public Collection<Buch> sucheNachAutor(String autor) {
		ArrayList<Buch> temp = new ArrayList<Buch>();

		for (Buch b : liste) {
			if (b.getAutor() == autor)
				temp.add(b);
		}

		return temp;
	}

	public Buch sucheNachISBN(String isbn) {
		Buch temp = null;
		for (Buch b : liste) {
			if (b.getIsbn().equals(isbn))
				return b;
		}
		return temp;
	}

	public Collection<Buch> bestandSortierenNach(Comparator<Buch> buch)

	{
		ArrayList<Buch> temp = liste;

		Collections.sort(temp, buch);

		return temp;
	}

	public String toString() {
		String temp = "";
		for (Buch b : liste) {
			temp += b.toString() + "\n";
		}
		return temp;
	}

	public Map<String, ArrayList<String>> bestandNachAutor() {

		Map<String, ArrayList<String>> temp = new HashMap<String, ArrayList<String>>();

		for (Buch b : liste) {
			// Überprüfen ob Autor schon in Map vorhanden ist
			if (!temp.containsKey(b.getAutor())) {
				// Falls nicht neuen Eintrag anlegen und Titel in ArrayList
				// hinzufuegen
				ArrayList<String> temp2 = new ArrayList<String>();
				temp2.add(b.getTitel());
				// Hinzufuegen zur Map
				temp.put(b.getAutor(), temp2);
			} else {
				// Titel zur ArrayList der Map hinzufuegen
				temp.get(b.getAutor()).add(b.getTitel());
			}
		}

		return temp;

	}
}
