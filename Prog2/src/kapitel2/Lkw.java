package kapitel2;

public class Lkw extends Fahrzeug{

	double ladung,maxLadung;

	public Lkw(String name,double ladung, double maxLadung) {
		super(name);
		super.setA(0.5);
		super.setHoechstgeschwindigkeit(105);
		this.ladung = ladung;
		this.maxLadung = maxLadung;
		
	}
	
	public void beladen(double ladungInTonnen)
	{
		if(ladungInTonnen >= maxLadung)
			ladung = maxLadung;
		else
			ladung += ladungInTonnen;
	}
	
	public void entladen(double ladungInTonnen)
	{
		if(ladung - ladungInTonnen > 0)
			ladung = 0;
		else
			ladung -= ladungInTonnen;
	}
	
	public boolean istVollBeladen()
	{
		return ladung==maxLadung;
	}
	
public double stoppen()
	{
		double bremsweg;
		if(istVollBeladen())
			bremsweg = 0.5 * (super.getGeschwindigkeit()/3.6 * super.getGeschwindigkeit()/3.6)/3 + super.getGeschwindigkeit()/36 * 3;
		else
			bremsweg = 0.5 * (super.getGeschwindigkeit()/3.6 * super.getGeschwindigkeit()/3.6)/5 + super.getGeschwindigkeit()/36 * 3; 
		return bremsweg;
	}

}
