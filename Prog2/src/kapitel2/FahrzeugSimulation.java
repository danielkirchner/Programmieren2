package kapitel2;

public class FahrzeugSimulation {
	
	public static void main(String args[])
	{
		Pkw pkw1 = new Pkw("Auto1");
		pkw1.beschleunigen(10);
		System.out.println(pkw1.getGeschwindigkeit());
		System.out.println(pkw1.getPosition());
		pkw1.fahren(60);
		System.out.println(pkw1.getGeschwindigkeit());
		System.out.println(pkw1.getPosition());
		
	}
}
