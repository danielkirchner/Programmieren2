package kapitel2;

public class DozentenverwaltungTest {

	public static void main(String[] args) {
		Person personen[] = new Person[5];
		Student aStudent = new Student(96450, 1000, "Peter", "M�ller", "Hauptstrasse", "4a", "Coburg", 455555);
		Student bStudent = new Student(96450, 1000, "Peter", "M�ller", "Hauptstrasse", "4a", "Coburg", 455555);
		Student cStudent = new Student(96450, 1000, "Peter", "M�ller", "Hauptstrasse", "4a", "Coburg", 455555);
		Student dStudent = new Student(96450, 1000, "Peter", "M�ller", "Hauptstrasse", "4a", "Coburg", 455555);
		Dozent aDozent = new Dozent(96450, 1000, "Hans", "Meyer", "Janaerstrasse", "21", "Coburg","Betriebssysteme");
	
		personen[0] = aStudent;
		personen[1] = aDozent;
		personen[2] = bStudent;
		personen[3] = cStudent;
		personen[4] = dStudent;
		
		for(Person p: personen)
		{
			System.out.print(p);
			System.out.println(", Gehalt:" + p.gehalt());
		}
	}

}
