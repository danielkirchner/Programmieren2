package kapitel2;

public class GlitterStar extends Star{
	public GlitterStar(double weight) throws IllegalArgumentException
	{
		super(weight);
	}

	public void decorate(){
		super.setWeight(weight() + 1);
	}
}
