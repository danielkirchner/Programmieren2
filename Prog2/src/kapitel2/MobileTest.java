package kapitel2;

public class MobileTest {

	public static void main(String[] args) {
		try{
		Star s1 = new Star(2);
		GlitterStar s2 = new GlitterStar(4);
		Star s3 = new Star(9);
		Wire w1 = new Wire(s1,s2);
		Wire w2 = new Wire(w1,s3);
		
		w1.balance();
		w2.balance();
		
		System.out.println(w2.toString());
		
		s2.decorate();
		s2.decorate();
		s2.decorate();
		
		w1.balance();
		w2.balance();
		
		System.out.println(w2.toString());
		}
		catch(IllegalArgumentException e)	
		{
			System.out.println("Illegal Value for Weight");
		}

		

	}

}
