package kapitel2;

public class Dozent extends Person {

	private String lehrgebiet;
	
	public Dozent(int postleitzahl, int gehalt, String vorname, String nachname, String strassenname, String hausnummer,
			String ort,String lehrgebiet) {
		super(postleitzahl, gehalt, vorname, nachname, strassenname, hausnummer, ort);
		this.lehrgebiet = lehrgebiet;
	}

	public String toString() {
		return "PLZ: " + super.getPostleitzahl()
			+ " Vorname: " + super.getVorname()
			+ " Nachname: " + super.getNachname()
			+ " Strassenname: " + super.getStrassenname()
			+ " Hausnummer: " + super.getHausnummer()
			+ " Ort: " + super.getOrt()
			+ " Lehrgebiet " + this.lehrgebiet;
	}

	public String getLehrgebiet() {
		return lehrgebiet;
	}

	public void setLehrgebiet(String lehrgebiet) {
		this.lehrgebiet = lehrgebiet;
	}

	public int gehalt() {
		return super.getGehalt();
	}

}
