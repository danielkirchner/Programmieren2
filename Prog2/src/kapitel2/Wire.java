package kapitel2;

public class Wire implements Mobile{
	private double weight;
	private double lengthLeft,lengthRight;
	Mobile mobileLeft,mobileRight;
	
	
	public Wire(Mobile mobileLinks, Mobile mobileRechts)
	{
		this.weight = mobileLinks.weight() + mobileRechts.weight();
		this.mobileLeft = mobileLinks;
		this.mobileRight = mobileRechts;
	}


	public double weight()
	{
		return weight;
	}
	public String toString()
	{
		return "[" + lengthLeft + "," + mobileLeft.toString() + " | " + lengthRight + "," + mobileRight.toString() + "]";
	}
	public void balance()
	{
		lengthRight = 1;
		while(!isBalanced())
		{
			lengthRight += 1;
			lengthLeft = lengthRight * mobileRight.weight()/mobileLeft.weight();
		}
	}
	
	private boolean isBalanced()
	{
		return lengthLeft * mobileLeft.weight() == lengthRight * mobileRight.weight();
	}

}
