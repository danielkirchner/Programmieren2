package kapitel2;

public abstract class Person {
	private int postleitzahl,gehalt;
	private String vorname,nachname,strassenname,hausnummer,ort;
	
	
	public abstract String toString();
	public abstract int gehalt();
	
	public Person(	int postleitzahl,
					int gehalt,
					String vorname,
					String nachname,
					String strassenname,
					String hausnummer,
					String ort)
	{
		this.postleitzahl = postleitzahl;
		this.vorname = vorname;
		this.nachname = nachname;
		this.strassenname = strassenname;
		this.hausnummer = hausnummer;
		this.ort = ort;
		this.gehalt = gehalt;
	}
	
	public int getPostleitzahl() {
		return postleitzahl;
	}
	public void setPostleitzahl(int postleitzahl) {
		this.postleitzahl = postleitzahl;
	}
	public int getGehalt() {
		return gehalt;
	}
	public void setGehalt(int gehalt) {
		this.gehalt = gehalt;
	}
	public String getVorname() {
		return vorname;
	}
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	public String getNachname() {
		return nachname;
	}
	public void setNachname(String nachname) {
		this.nachname = nachname;
	}
	public String getStrassenname() {
		return strassenname;
	}
	public void setStrassenname(String strassenname) {
		this.strassenname = strassenname;
	}
	public String getHausnummer() {
		return hausnummer;
	}
	public void setHausnummer(String hausnummer) {
		this.hausnummer = hausnummer;
	}
	public String getOrt() {
		return ort;
	}
	public void setOrt(String ort) {
		this.ort = ort;
	}
}
