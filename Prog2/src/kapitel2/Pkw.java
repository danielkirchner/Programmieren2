package kapitel2;

public class Pkw extends Fahrzeug{


	public Pkw(String name) {
		super(name);
		super.setA(2.0);
		super.setHoechstgeschwindigkeit(160);
	}
	
	public double stoppen(){
		double bremsweg = 0.5 * (super.getGeschwindigkeit()/36)*(super.getGeschwindigkeit()/36) + (super.getGeschwindigkeit()/36) * 3;	
		return bremsweg;
	}

	

}
