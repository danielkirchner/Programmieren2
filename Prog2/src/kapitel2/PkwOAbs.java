package kapitel2;

public class PkwOAbs extends Pkw {

	public PkwOAbs(String name) {
		super(name);
		super.setA(2.0);
		super.setHoechstgeschwindigkeit(140);
	}
	
	public double stoppen()
	{
		double bremsweg = (super.getGeschwindigkeit()/36)*(super.getGeschwindigkeit()/36)+(super.getGeschwindigkeit()/36)*3;
		return bremsweg;	
	}

}
