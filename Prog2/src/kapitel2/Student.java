package kapitel2;

public class Student extends Person
{
	private int matrikelnummer;
	
	public Student(int postleitzahl, int gehalt, String vorname, String nachname, String strassenname,
			String hausnummer, String ort,int matrikelnummer) 
	{
		super(postleitzahl, gehalt, vorname, nachname, strassenname, hausnummer, ort);
		this.matrikelnummer = matrikelnummer;
	}


	public String toString() {
		return "PLZ: " + super.getPostleitzahl()
		+ " Vorname: " + super.getVorname()
		+ " Nachname: " + super.getNachname()
		+ " Strassenname: " + super.getStrassenname()
		+ " Hausnummer: " + super.getHausnummer()
		+ " Ort: " + super.getOrt()
		+ " Matrikelnummer: " + this.matrikelnummer;
	}

	public int gehalt() {
		return super.getGehalt();
	}


	public int getMatrikelnummer() {
		return matrikelnummer;
	}


	public void setMatrikelnummer(int matrikelnummer) {
		this.matrikelnummer = matrikelnummer;
	}

	

	
	
}
