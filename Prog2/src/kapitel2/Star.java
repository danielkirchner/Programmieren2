package kapitel2;

public class Star implements Mobile {

	private double weight;

	public Star(double weight) throws IllegalArgumentException {
		if (weight < 0) {
			throw new IllegalArgumentException();
		} else {
			this.weight = weight;
		}
	}

	public double weight() {

		return weight;
	}

	public void balance() {
	}
	
	public String toString(){
		return "" + weight();
	}
	
	public void setWeight(double weight){
		this.weight = weight;
	}

}
