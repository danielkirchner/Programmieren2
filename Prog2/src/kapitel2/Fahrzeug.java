package kapitel2;

public abstract class Fahrzeug {
	private double position;			//in km
	private double geschwindigkeit; 	//in km/h
	private String name;
	private double a;					//in m/s^2
	private int hoechstgeschwindigkeit;
	
	public Fahrzeug(String name)
	{
		position = 0;
		geschwindigkeit = 0;
		this.name = name;
	}
	
	
	public double beschleunigen(double sekunden) {
		//Gibt neue Position zur�ck
		if(3.6 * a * sekunden <= hoechstgeschwindigkeit)
		{
			geschwindigkeit = 3.6 * a * sekunden;
			position += a * sekunden * sekunden / 2000;
		}
		else
		{
			geschwindigkeit = hoechstgeschwindigkeit;
			position += sekunden/3600 * hoechstgeschwindigkeit;
		}
			
		
		return position;
	}

	public double hoechstGeschwindigkeit() {
		return hoechstgeschwindigkeit;
	}

	public double fahren(double minuten) {
		//Gibt Position zur�ck
		position += geschwindigkeit * minuten/60;
		return position;
	}
	
	public abstract double stoppen();
	
	//Getters and Setters
	public double getA() {
		return a;
	}
	public void setA(double a) {
		this.a = a;
	}
	public int getHoechstgeschwindigkeit() {
		return hoechstgeschwindigkeit;
	}
	public void setHoechstgeschwindigkeit(int hoechstgeschwindigkeit) {
		this.hoechstgeschwindigkeit = hoechstgeschwindigkeit;
	}
	
	public double getPosition() {
		return position;
	}

	public void setPosition(double position) {
		this.position = position;
	}

	public double getGeschwindigkeit() {
		return geschwindigkeit;
	}

	public void setGeschwindigkeit(double geschwindigkeit) {
		this.geschwindigkeit = geschwindigkeit;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
