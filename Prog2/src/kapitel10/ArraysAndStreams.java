package kapitel10;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class ArraysAndStreams {
public static void main(String[] args) {
int[] values = { 2, 9, 5, 0, 3, 7, 1, 4, 8, 6 };

// display original values
System.out.printf("Original values: %s%n", Arrays.asList(values));
// Ausgabe aller Werte von values in aufsteigender Reihenfolge
//System.out.println(Arrays.toString(liste.stream().sorted().toArray()));
System.out.println(Arrays.toString(IntStream.of(values).sorted().toArray()));
IntStream.of(values).sorted().forEach(Math::incrementExact);
// Ausgabe aller Werte in values, die gr��er als 4 sind
System.out.println();
IntStream.of(values).filter(x-> x>4).forEach(System.out::print);;
// Filtern von allen Werten in values, die gr��er als 4 sind, und // sortierte diese anschlie�end
//neu
}
}
