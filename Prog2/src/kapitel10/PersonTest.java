package kapitel10;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;


public class PersonTest {

	public static void main(String[] args) {
		Person[] people = new Person[] { new Person("Ted", "Neward", 41),
				new Person("Charlotte", "Neward", 41),
				new Person("Michael", "Neward", 19),
				new Person("Matthew", "Neward", 13),
				new Person("Adam", "Pfeiffer", 43) };
		List<Person> liste = Arrays.asList(people);
		
		//Methode 1 - CompareTo
//		for(Person p:people)
//		{
//			System.out.println(p.toString());
//		}
//		Arrays.sort(people);
//		System.out.println("---");
//		
//		for(Person p:people)
//		{
//			System.out.println(p.toString());
//		}

		
		
		//Methode 2 - Lambdaausdruck
//		liste.forEach(p->System.out.println(p.toString()));
//		liste.sort((Person p1,Person p2)->p1.getFirstName().compareTo(p2.getFirstName()));
//		
//		System.out.println("---");
//		
//		liste.forEach(p->System.out.println(p.toString()));
		
		//Methode 3 - Comparable.comparing
		liste.forEach(p->System.out.println(p.toString()));
		liste.sort(Comparator.comparing(Person::getFirstName));
		
		System.out.println("---");
		liste.forEach(p->System.out.println(p.toString()));
		
		
		
	}

}
