package kapitel10;

class Person implements Comparable<Person>{
	private final String firstName;
	private String lastName;
	private final int age;

	public Person(String f, String l, int a) {
		firstName = f;
		lastName = l;
		age = a;
	}

	public String toString() {
		return "Person [firstName=" + firstName + ", lastName=" + lastName + ", age=" + age + "]";
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public int getAge() {
		return age;
	}

	@Override
	public int compareTo(Person o) {
		return this.firstName.compareTo(o.getFirstName());
	}
	
	
	
}
