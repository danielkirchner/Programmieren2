package kapitel10;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class Bibiliothek {

	ArrayList<Buch> liste;

	public Bibiliothek() {
		liste = new ArrayList<Buch>();
	}

	public boolean einfuegen(Buch b) {
		return liste.add(b);
	}

	
	
	public Collection<Buch> sucheNachAutor(String autor) {
			
		return liste.stream().filter(b->b.getAutor().equals(autor)).collect(Collectors.toList());
	}

	public Buch sucheNachISBN(String isbn) {
		return (Buch)liste.stream().filter(b->b.getIsbn().equals(isbn)).toArray()[0];
	}

	public Collection<Buch> bestandSortierenNach(Comparator<Buch> buch)

	{
		ArrayList<Buch> temp = liste;

		Collections.sort(temp, buch);

		return temp;
	}

	public String toString() {
		String temp = "";
		for (Buch b : liste) {
			temp += b.toString() + "\n";
		}
		return temp;
	}

	public Map<String, List<Buch>> bestandNachAutor() {

		return liste.stream().collect(Collectors.groupingBy(Buch::getAutor));

	}
}
