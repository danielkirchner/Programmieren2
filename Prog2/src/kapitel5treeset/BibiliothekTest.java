package kapitel5treeset;

public class BibiliothekTest {

	public static void main(String[] args) {
		
		Bibiliothek b = new Bibiliothek();
		
		Buch bu1 = new Buch("Arnold","1","Inspirierender Titel");
		Buch bu2 = new Buch("Bastian","2","Abstoßender Titel");
		Buch bu3 = new Buch("Casten","3","Interessanter Titel");
		Buch bu4 = new Buch("D","4","Uninteressanter Titel");
		Buch bu5 = new Buch("Arnold","5","Harry Potter");
		
		b.einfuegen(bu1);
		b.einfuegen(bu3);
		b.einfuegen(bu2);
		b.einfuegen(bu4);
		b.einfuegen(bu5);
		
		System.out.println(b.toString());
		
		System.out.println(b.bestandNachAutor());
		
	}

}
