package kapitel5treeset;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class Bibiliothek {

	TreeSet<Buch> tree;

	public Bibiliothek() {
		tree = new TreeSet<Buch>();
	}

	public void einfuegen(Buch b) {
		tree.add(b);
	}

	public Collection<Buch> sucheNachAutor(String autor) {
		ArrayList<Buch> temp = new ArrayList<Buch>();

		for (Buch b : tree) {
			if (b.getAutor() == autor)
				temp.add(b);
		}

		return temp;
	}

	public Buch sucheNachISBN(String isbn) {
		Buch temp = null;
		for (Buch b : tree) {
			if (b.getIsbn() == isbn)
				return b;
		}
		return temp;
	}

	public Collection<Buch> gibAus() {
		return tree;
	}

	public String toString() {
		String temp = "";
		for (Buch b : tree) {
			temp += b.toString() + "\n";
		}
		return temp;
	}

	public Map<String, ArrayList<String>> bestandNachAutor() {
		Map<String, ArrayList<String>> temp = new HashMap<String, ArrayList<String>>();

		for (Buch b : tree) {
			if (!temp.containsKey(b.getAutor())) {
				ArrayList<String> temp2 = new ArrayList<String>();
				temp2.add(b.getTitel());
				temp.put(b.getAutor(), temp2);
			} else {
				temp.get(b.getAutor()).add(b.getTitel());
			}
		}

		return temp;

	}
}
