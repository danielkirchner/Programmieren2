package kapitel5treeset;

public class Buch implements Comparable<Buch>{

	private String autor, isbn, titel;

	public Buch(String autor, String isbn, String titel) {
		this.autor = autor;
		this.isbn = isbn;
		this.titel = titel;
	}
	
	//toString
	public String toString(){
		return "Autor: " + autor + ", ISBN: "+ isbn + ", Titel:" + titel;
	}

	
	
	
	//Getters and Setters
	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}
	
	public int compareTo(Buch b) {
		if(b.getIsbn().compareTo(this.getIsbn())==1)
			return -1;
		if(b.getIsbn().compareTo(this.getIsbn())==0)
			return 0;
		return 1;
	}

	
}
