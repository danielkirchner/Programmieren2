package kapitel0;

public class Zaehler {
	int einer,zehner;
	
	public Zaehler()
	{
		einer = 0;
		zehner = 0;
	}
	
	public void erhoeheUmEins() throws EinerUeberlauf
	{
		einer += 1;
		if (einer >= 10)
		{
			einer = 0;
			throw new EinerUeberlauf();		
		}
	}
	
	public void erhoeheUmZehn() throws Ueberlauf
	{
		zehner += 1;
		if (zehner >= 10)
		{
			throw new Ueberlauf();
		}
	}
}
