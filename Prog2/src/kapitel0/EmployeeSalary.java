package kapitel0;

import java.util.*;
/**
 * @class Das folgende Programm liest von der Console die Angestelltennummer, den Stundenlohn und die 
 * w�chentliche Arbeitszeit ein. Anschliessend wird der Name, w�chentliche Verdienst sowie die �berstunden
 * ausgegeben.
 */

public class EmployeeSalary {
	
	

	public static void main(String[] args) {
		Scanner scnr = new Scanner(System.in);
		long employeeNumber = 0;
		String employeeName;
		double hourlySalary = 0.00;
		double weeklyTime = 0.00;
		double regularTime, overtime;
		double regularPay, overtimePay, netPay;

		System.out.print("Enter Employee Number (00000): ");
		try
		{
		employeeNumber = scnr.nextLong();
		}
		catch(InputMismatchException e)
		{
			System.out.println("Keine g�ltige Eingabe (Format beachten).");
		}
		catch(Exception e)
		{
			System.out.println("Internal Error.");
		}
		finally
		{
			scnr = new Scanner(System.in);
		}
		if(employeeNumber < 0)
		{
			System.out.println("Keine g�ltige Eingabe (Keine negativen Zahlen erlaubt).");
		}
		

		if (employeeNumber == 82500)
			employeeName = "Peter Baker";
		else if (employeeNumber == 92746)
			employeeName = "John Kouma";
		else if (employeeNumber == 54080)
			employeeName = "Steg Larson";
		else if (employeeNumber == 86285)
			employeeName = "Gertrude Monay";
		else
			employeeName = "Unknown";

		try
		{
		System.out.print("Enter Hourly Salary: ");
		hourlySalary = scnr.nextDouble();
		}
		catch(InputMismatchException e)
		{
			System.out.println("Keine g�ltige Eingabe.");
		}
		catch(Exception e)
		{
			System.out.println("Internal Error.");
		}
		finally
		{
			scnr = new Scanner(System.in);
		}
		if(hourlySalary < 0)
		{
			System.out.println("Der Stundenlohn kann nicht negativ sein.");
		}

		
		
		try
		{
			System.out.print("Enter Weekly Time: ");
			weeklyTime = scnr.nextDouble();
		}
		catch(InputMismatchException e)
		{
			System.out.println("Keine g�ltige Eingabe.");
		}
		catch(Exception e)
		{
			System.out.println("Internal Error.");
		}
		finally
		{
			scnr = new Scanner(System.in);
		}
		if(weeklyTime < 0)
		{
			System.out.println("Die w�chentliche Arbeitszeit kann nicht negativ sein.");
		}
		
	
		if (weeklyTime < 40) {
			regularTime = weeklyTime;
			overtime = 0;
			regularPay = hourlySalary * regularTime;
			overtimePay = 0;
			netPay = regularPay;
		} else {
			regularTime = 40;
			overtime = weeklyTime - 40;
			regularPay = hourlySalary * 40;
			overtimePay = hourlySalary * overtime;
			netPay = regularPay + overtimePay;
		}

		System.out.println("======================");
		System.out.println("==-=-= Employee Payroll =-=-==");
		System.out.println("-------------------------------------------");
		System.out.printf("Employee #:    %d\n", employeeNumber);
		System.out.printf("Employee Name: %s\n", employeeName);
		System.out.printf("Hourly Salary: %.2f\n", hourlySalary);
		System.out.printf("Weekly Time:   %.2f\n", weeklyTime);
		System.out.printf("Regular Pay:   %.2f\n", regularPay);
		System.out.printf("Overtime Pay:  %.2f\n", overtimePay);
		System.out.printf("Total Pay:     %.2f\n", netPay);
		System.out.println("======================");
	}

}
