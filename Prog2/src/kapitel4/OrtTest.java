package kapitel4;

public class OrtTest {

	public static void main(String[] args) {
		Ort<String> einOrt = new Ort<String>(1);
		System.out.println(einOrt.istBelegt());
		einOrt.hinzufuegen("OO-Softwareentwicklung");
		System.out.println(einOrt.istBelegt());
		System.out.println(einOrt.entnehmen());
		System.out.println(einOrt.istBelegt());
		System.out.println(einOrt.entnehmen());

	}

}
