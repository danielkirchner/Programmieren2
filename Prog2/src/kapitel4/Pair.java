package kapitel4;

public class Pair<E, E2> {
	E a;
	E2 b;

	public Pair(E a, E2 b) {
		this.a = a;
		this.b = b;
	}

	public String toString() {
		return "("+ a.toString() + ", "+b.toString() + ")";
	}

	public boolean equals(Object o1) {
		if (o1.getClass() != this.getClass()) {
			return false;
		} else {
			Pair<E,E2> ptemp = (Pair<E,E2>) o1;
			if (this.a.equals(ptemp.a) && this.b.equals(ptemp.b)) {
				return true;
			}
		}

		return false;

	}
}
