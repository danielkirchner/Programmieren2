package kapitel4;

public class Ort<E>{
	
	int ortsId;
	E element;

		public Ort(int ortsId)
		{
			this.ortsId = ortsId;
		}
		
		public E entnehmen()
		{
			E temp = element;
			element = null;
			return temp;
		}
			
		public void hinzufuegen(E e)
		{
			if(element == null)
			{
				element = e;
			}
		}
		
		public E getEingelagertesElement()
		{
			return element;
		}
		
		public boolean istBelegt()
		{
			if(element!=null)
			{
				return true;
			}
			return false;
		}
		
		public int getOrtsId(){
			return ortsId;
		}
		
		public String toString()
		{
			return element.toString();
		}
}
