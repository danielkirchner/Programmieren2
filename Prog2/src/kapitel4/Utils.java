package kapitel4;

public class Utils<E extends Comparable<E>> {

	public static <E> void print(E[] a) {
		String s = "";
		s += "[";
		for (E e : a) {
			s += e.toString() + ",";
		}
		s += "]";
		System.out.println(s);
	}

	public static <E extends Comparable<E>> void sortiere(E[] a) {
		for (int i = 0; i < a.length; i++) {
			for (int j = 1; j < (a.length - i); j++) {

				if (a[j - 1].compareTo(a[j]) > 0) {
					// swap the elements!
					E temp = a[j - 1];
					a[j - 1] = a[j];
					a[j] = temp;
				}

			}
		}
	}
	
	public static <E> E noNull(E...parameter){
		for(E o:parameter)
		{
			if(o!=null)
			return o;
			
			
		}
		return null;
	}

}
